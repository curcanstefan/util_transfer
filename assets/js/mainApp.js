var customTime = {
    format: function(inputDate, format){
        var time = ('0' + inputDate.getHours()).slice(-2) + ':' + ('0' + inputDate.getMinutes()).slice(-2);
        var date = ('0' + inputDate.getDate()).slice(-2) + '-' + ('0' + (inputDate.getMonth()+1)).slice(-2) + '-' + inputDate.getFullYear();
        var serverDate = inputDate.getFullYear() + '-' + ('0' + (inputDate.getMonth()+1)).slice(-2) + '-' + ('0' + inputDate.getDate()).slice(-2);
        switch(format){
            case 'time':
                return time;
            case 'date':
                return date;
            case 'serverDate':
                return serverDate;
            case 'datetime':
                return date + ' ' + time;
        }
    },
    dateFromString: function(stringDate) {
        var parts = stringDate.split(/ |-|:/);
        var year  = parseInt(parts[0]);
        var month = parseInt(parts[1])-1;
        var day   = parseInt(parts[2]);
        var hour  = parseInt(parts[3]);
        var min   = parseInt(parts[4]);
        var sec   = parseInt(parts[5]);
        return new Date(year, month, day, hour, min, sec);
    }
};

var scrollToDetails = function(){
    $('html, body').animate({
        scrollTop: $("#tripDetailsTitle").offset().top - 50
        }, 500);
};

$(document).ready(function(){
    schedule.initData();

    $(window).on('hashchange', function(){
        /* global valueSet */
        /* global dataManager */
        /* global schedule */
        var hash = (location.hash.replace( /^#/, '' ) || 'blank');
        if(!hash) return;
        valueSet    = hash.split('/');
        var action  = valueSet[0];
        var val1    = valueSet[1];
        var val2    = valueSet[2];
        switch(action) {
            case 'trip':
                scrollToDetails();
                dataManager.getTrips(val1, schedule.displayTripDetails);
                break;
            case 'editTrip':
                if (parseInt(val1)) {
                    dataManager.getTrips(val1, function(data) {
                        schedule.editTrip(val1, data[0]);
                    });
                } else {
                    schedule.editTrip(val1, val2);
                }
                scrollToDetails();
                break;
            case 'addTraveler':
                dataManager.getTrips(val1, function(data) {
                    schedule.displayTripDetails(data);
                    schedule.editTraveler(val1);
                    $('html, body').animate({
                        scrollTop: $("#addTraveler").parent().offset().top - 50
                        }, 500);
                });
                break;
            case 'editTraveler':
                schedule.editTraveler(val1, val2);
                $('html, body').animate({
                    scrollTop: $("#addTraveler").parent().offset().top - 50
                    }, 500);
                break;
        }
    });

    }
);
