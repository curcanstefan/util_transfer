var trips = [];
var currentTrip = null;

var schedule = {
    newTrip: function(
        id, title,
        startDate, endDate,
        routeId, routeName,
        driverId, driverName,
        vehicleId, vehicleName, vehicleCapacity,
        travelers){
        var trip = {};
        trip.id = id;
        trip.title = title;
        if (typeof startDate == 'object') {
            trip.startDate = startDate;
        }
        if (typeof startDate == 'string') {
            trip.startDate = customTime.dateFromString(startDate);
        }
        if (typeof endDate == 'object') {
            trip.endDate = endDate;
        }
        if (typeof endDate == 'string') {
            trip.endDate = customTime.dateFromString(endDate);
        }
        trip.routeId = routeId;
        trip.routeName = routeName;
        trip.driverId = driverId;
        trip.driverName = driverName;
        trip.vehicle_id = vehicleId;
        trip.vehicleName = vehicleName;
        trip.vehicleCapacity = vehicleCapacity;
        if(travelers instanceof Array) trip.travelers = travelers;
        else if(travelers instanceof Object) trip.travelers = schedule.newTraveler(travelers);

        return trip;
    },
    newTripFromObject: function(trip){
        return schedule.newTrip(
            trip.id,
            trip.title,
            trip.start_date,
            trip.end_date,
            trip.route_id,
            trip.route_name,
            trip.driver_id,
            trip.driver_name,
            trip.vehicle_id,
            trip.vehicle_name,
            trip.vehicle_capacity,
            trip.travelers);
    },
    newTripFromArray: function(trips){
        var result = [];
        $(trips).each(function(id, trip) {
            result.push(
                schedule.newTripFromObject(trip)
            );
        });
        return result;
    },
    addTripsToCache: function(newTrips) {
        $(newTrips).each(function(key, trip) {
            trips[trip.id] = schedule.newTripFromObject(trip);
        });
    },
    initData: function(){
        schedule.getTripsByDate(new Date());
        /*dataManager.getTrips(null, function(data){
            trips = trips.concat(data);
        });*/
    },
    newTraveler: function(input){
        return {
            id:         input['id'],
            name:       input['name'],
            phone:      input['phone'],
            reputation: input['reputation'],
            totalPrice: input['total_price'],
            paid:       input['paid'],
            seats:      input['seats'],
            fromCity:   input['from_city'],
            toCity:     input['to_city']
        };
    },
    getTripsByDate: function(searchDate){
        if(!(searchDate instanceof Date)) return [];
        return trips.filter(function(el){
            return (el.startDate.getDate() == searchDate.getDate()
            && el.startDate.getMonth() == searchDate.getMonth()
            && el.startDate.getFullYear() == searchDate.getFullYear());
            });
    },
    getTripById: function(id) {
        var result = trips.filter(function(el){
            return (el.id == id);
            });
        return result.length ? result[0] : null;
    },
    displayTripDetails: function(trip) {
        trip = trip[0];
        /*global currentTrip*/
        currentTrip = trip;
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(trip.title);

        $(trip.travelers).each(function(id, traveler) {
            traveler.trip_id = trip.id;
            traveler.reputationColor = 'default';
            if(traveler.reputation < 0) traveler.reputationColor = 'danger';
            if(traveler.reputation > 0) traveler.reputationColor = 'success';
            traveler.paidColor = 'warning';
            if(traveler.paid == traveler.total_price) traveler.paidColor = 'success';
        });

        trip.displayStartDate = customTime.format(trip.startDate, 'datetime');
        trip.displayEndDate = customTime.format(trip.endDate, 'datetime');
        trip.occupiedSeats = trip.travelers.length;

        var tripTemplate = $('.xtemplate#displayTrip').html();
        var tripDetails = templates.render(tripTemplate, trip);

        container.html(tripDetails);
    },
    editTrip: function(id, date) {
        /* global templates */
        var trip = schedule.newTrip();
        var newTrip = true;

        if (parseInt(id)) {
            trip = date;
            newTrip = false;
        }

        var template = $('.xtemplate#editTrip').html();
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(trip.title || dictionary.get('new trip title'));
        container.html(templates.render(template));

        // get vehicles and populate select options
        var optionTemplate = $('.xtemplate#editTripVehicleOption').html();
        dataManager.getVehicles(null, function(data) {
            var optionContainer = container.find('select#vehicle');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored title
            container.find('#title').val(trip.title);
            // set stored vehicle
            if (trip.vehicle_id) {
                optionContainer.val(trip.vehicle_id);
            }
        });

        // get routes and populate select options
        dataManager.getRoutes(null, function(data) {
            var optionContainer = container.find('select#route');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored vehicle
            if (trip.route_id) {
                optionContainer.val(trip.route_id);
            }
        });

        // get drivers and populate select options
        dataManager.getDrivers(null, function(data) {
            var optionContainer = container.find('select#driver');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored vehicle
            if (trip.driver_id) {
                optionContainer.val(trip.driver_id);
            }
        });

        var datepickerSelector = $('#edit_trip #start_date');
        datepickerSelector.datepicker({
            format: "dd-mm-yyyy",
            startDate: "01-01-2015",
            weekStart: 1,
            todayBtn: "linked",
            language: "ro",
            todayHighlight: true/*,
            datesDisabled: ['06/06/2015', '06/21/2015']*/
        });
        // set stored date
        if (!newTrip) {
            datepickerSelector.datepicker('setDate', customTime.format(trip.startDate, 'date'));
            container.find('#start_time').val(customTime.format(trip.startDate, 'time'));
            container.find('#id').val(trip.id);
        } else if (date) {
            datepickerSelector.datepicker('setDate', date);
        }
        // add functionality to save button
        container.find('.save-trip').on('click',
            function(event) {
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addTrip(formValues, function(data) {
                    var tripId = newTrip ? data.responseText : trip.id;
                    location.hash = '#trip/' + tripId;
                });
                // add trip to js store
            }
        );
        // add functionality to save button
        container.find('.cancel-trip').on('click',
            function(event) {
                location.hash = '#trip/' + trip.id;
            }
        );
    },

    editTraveler: function(tripId, travelerId) {
        var isNewTraveler = false;
        if (travelerId && travelerId > 0) {
            isNewTraveler = true;
            var traveler = $.grep(currentTrip.travelers, function(n,i) {return n['id'] == travelerId;})[0];
        }
        var template = $('.xtemplate#addTravelerToTrip').html();
        var container = $("#tripDetails #addTraveler");
        var dataForTemplate = {
            'tripId': tripId
        };
        container.html(templates.render(template, dataForTemplate));

        container.addClass('well');

        // get drivers and populate select options
        dataManager.getTravelers(null, function(data) {
            var travelersNames = [];
            var travelersPhones = [];
            $(data).each(function(key, traveler) {
                travelersNames.push({
                    label: traveler.phone + ' ' + traveler.name,
                    value: traveler.name,
                    data: traveler
                });
                travelersPhones.push({
                    label: traveler.phone + ' ' + traveler.name,
                    value: traveler.phone,
                    data: traveler
                });
            });
            container.find('#traveler_name').autocomplete({
                source: travelersNames,
                select: function(event, row) {
                    container.find('#phone').val(row.item.data.phone);
                }
            });
            container.find('#phone').autocomplete({
                source: travelersPhones,
                select: function(event, row) {
                    container.find('#traveler_name').val(row.item.data.name);
                }
            });
        });

        // seats spinner
        container.find('#seats').spinner({min: 1, max: 20});
        container.find('#seats').spinner('value', 1);

        //container.find('#id').val(tripRowId);
        container.find('#trip_id').val(tripId);

        // complete known values
        if (isNewTraveler) {
            container.find('#traveler_id').val(traveler.id);
            container.find('#traveler_name').val(traveler.name);
            container.find('#phone').val(traveler.phone);
            container.find('#seats').val(traveler.seats);
            container.find('#departure_city').val(traveler.from_city);
            container.find('#destination_city').val(traveler.to_city);
            container.find('#total_price').val(traveler.total_price);
            container.find('#amount_paid').val(traveler.paid);
        }

        // add functionality to save button
        container.find('.save-traveler').on('click',
            function(event) {
                container.removeClass('well');
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addTravelerToTrip(formValues, function(data) {
                    location.hash = '#trip/' + tripId;
                });
                // add trip to js store
            }
        );

    }
};
