var dictionary = {
    availableLanguages: ['ro'],
    language: 'ro',
    setLanguage: function(id) {
        if(this.availableLanguages.indexOf(id) > -1) {
            this.language = id;
        }
    },
    get: function(id) {
        return dictionarySet[this.language][id] || id;
    }
};

var dictionarySet = {
    'ro': {
        'no trips for this day':        'Nu sunt inregistrari pe aceasta zi',

        'departure time':               'Pleaca la ',
        'arrival time':                 'Ajunge la ',
        'vehicle':                      'Transport cu ',
        'seats':                        'loc(uri)',
        'total seats':                  'Locuri in masina',
        'free seats':                   'Locuri libere',
        'occupied seats':               'Locuri ocupate',
        'travelers':                    'Calatori',
        'add new trip':                 'Adauga transport',
        'new trip title':               'Transport nou',
        'new trip title label':         'Titlu transport',
        'new trip title placeholder':   'titlu transport',
        'new trip date label':          'Ziua plecarii',
        'new trip date placeholder':    'alege o zi',
        'new trip time label':          'Ora plecarii',
        'new trip vehicle label':       'Masina',
        'route label':                  'Ruta',
        'driver label':                 'Sofer',
        'trip price':                   'Pret',
        'submit':                       'Ok',
        'submit new':                   'Salveaza',
        'cancel':                       'Anuleaza',
        'edit':                         'Modifica',

        'traveler name':                'Nume',
        'traveler phone':               'Numar telefon',
        'traveler seats':               'Locuri rezervate',
        'traveler departure':           'De la',
        'traveler destination':         'Pana la',
        'traveler total price':         'Pret total',
        'traveler paid':                'Achitat',
        'submit add traveler':          'Adauga',

        'paid':                         'platit',
        'ron':                          'RON',
    }
};
