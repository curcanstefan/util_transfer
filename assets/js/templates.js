var templates = {
    render: function(template, data) {
        var result = template;
        // for each
        result = result.replace(
            /{{each (.*?)}}([^]+?){{end each}}/g,
            function(fullMatch, id, match) {
                return templates.each(match, data[id]);
            });

        // translate
        result = result.replace(/{{trans}}(.*?){{end trans}}/g, templates.translate);

        // values
        result = result.replace(/{{(.*?)}}/g, function(fullMatch, match) {
            return data[match];
        });
        return result;
    },
    translate: function(fullMatch, match, offset) {
        return dictionary.get(match);
    },
    each: function(template, data) {
        var result = [];
        $(data).each(function(id, value) {
            result.push(templates.render(template, value));
        });
        return result.join('');
    }
};
;