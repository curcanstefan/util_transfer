var dataManager = {
    getTrips: function(id, callback) {
        var objectCallback = function(data) {
            var trips = schedule.newTripFromArray(data);
            if(typeof callback === 'function'){
                callback(trips);
            }
        }
        var key = 'trips' + (id || '');
        var cachedData = dataManager.getTripsFromCache(key, callback);
        if(cachedData !== 'undefined' && cachedData !== null){
            return objectCallback(cachedData);
        }

        return dataManager.getTripsFromServer(id, key, objectCallback);
    },
    getTripsFromCache: function(key) {
        return dataManager.getFromCache(key);
    },
    getTripsFromServer: function(id, key, callback) {
        dataManager.getFromServer(
            'Trips/get/' + (id || ''),
            key,
            function(data) {
                schedule.addTripsToCache(data);
                if (typeof callback === 'function') {
                    callback(data);
                }
            });
    },
    getVehicles: function(id, callback) {
        dataManager.getFromServer(
            'Vehicles/get/' + (id || ''),
            'vehicles' + (id || ''),
            callback);
    },
    getRoutes: function(id, callback) {
        dataManager.getFromServer(
            'Routes/get/' + (id || ''),
            'routes' + (id || ''),
            callback);
    },
    getDrivers: function(id, callback) {
        dataManager.getFromServer(
            'Drivers/get/' + (id || ''),
            'drivers' + (id || ''),
            callback);
    },
    getTravelers: function(id, callback) {
        dataManager.getFromServer(
            'Drivers/get/' + (id || ''),
            'travelers' + (id || ''),
            callback);
    },
    getFromCache: function(id) {
        return null;
        return JSON.parse(localStorage.getItem(id));
    },
    setToCache: function(key, data) {
        return false;
        return localStorage.setItem(key, JSON.stringify(data));
    },
    getFromServer: function(url, key, callback) {
        $.getJSON(url, function(data){
            dataManager.setToCache(key, data);
            if(typeof callback === 'function'){
                callback(data);
            }
        });
    },
    getTripsByDay: function(searchDate, callback) {
        /* global customTime */
        /* global schedule */
        var url = 'Trips/getByDay/'+customTime.format(searchDate, 'serverDate');

        $.getJSON(url, function(data){
            schedule.addTripsToCache(data);
            if(typeof callback === 'function'){
                callback(data);
            }
        });
    },
    addTrip: function(input, callback) {
        // add to server
        // add to cache
        var objectCallback = function(data, status) {
            if(typeof callback === 'function'){
                callback(data);
            }
        };
        dataManager.addTripToServer(input, objectCallback);
    },
    addTripToServer: function(trip, callback) {
        dataManager.addToServer(
            'Trips/set',
            trip,
            callback);
    },
    addTravelerToTrip: function(trip, callback) {
        dataManager.addToServer(
            'Travelers/set',
            trip,
            callback);
    },
    addToServer: function(url, data, callback) {
        var objectCallback = function(data, status){
            if(typeof callback === 'function'){
                callback(data);
            }
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            complete: objectCallback
        });
    }
};
