<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Tables.
| -------------------------------------------------------------------------
| Database table names.
*/
$config['tables']['travelers']  = 'travelers';
$config['tables']['trip_rows']  = 'trip_rows';
$config['tables']['trips']      = 'users';

/*
 | Users table column and Group table column you want to join WITH.
 |
 | Joins from users.id
 | Joins from groups.id
 */
$config['join']['travelers']    = 'traveler_id';
$config['join']['trips']        = 'trip_id';