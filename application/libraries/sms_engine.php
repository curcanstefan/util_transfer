<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//http://9sms.ro/api.php?apikey=4f68cdf33a1f534d522e42c61282ccf7&numar=[numarTelefon]&mesaj=[textMesaj]
class Sms_engine {
    public function sendSms($phone, $message) {
        if (!$message) {
            return false;
        }
        $message = substr($message, 0, 160);
        $api = 'http://9sms.ro/api.php?';
        $payload = [
            'apikey'    => '4f68cdf33a1f534d522e42c61282ccf7',
            'numar'     => $phone,
            'mesaj'     => base64_encode($message),
        ];
        return file_get_contents($api.http_build_query($payload));
        //return $api.http_build_query($payload);
    }
}
