<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// SendPulse's PHP Library: https://github.com/sendpulse/sendpulse-rest-api-php
require_once(APPPATH.'third_party/api/sendpulseInterface.php');
require_once(APPPATH.'third_party/api/sendpulse.php');

class Mail_engine {
    public function sendEmail($toEmail, $toName, $subject, $htmlBody, $plainBody) {
        $SPApiProxy = new SendpulseApi( '6b96f420ea00dd454ae1ee5cfc8809c1', 'ba75152dfc34d1f70aea131fca5cd42d', 'file' );
        $email = [
            'html'    => $htmlBody,
            'text'    => $plainBody,
            'subject' => $subject,
            'from'    => [
                'name'  => $toName,
                'email' => $toEmail,
            ],
            'to'      => [
                [
                    'name'  => 'Subscriber Name',
                    'email' => 'curcanstefan@gmail.com',
                ]
            ],
        ];
        return $SPApiProxy->smtpSendMail( $email );
    }
}
