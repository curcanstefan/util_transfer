<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Traveler Model
*
* Author:  Curcan Stefan
* 		   curcanstefan@gmail.com
*
* Created:  27.07.2015
*
* Last Change: 27.07.2015
*
* Changelog:
* * 27.07.2015 - Created
*/

class Driver_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('traveler', TRUE);
		//$this->load->helper('cookie');
		//$this->load->helper('date');

		//initialize db tables data
		//$this->tables  = $this->config->item('tables', 'traveler');

		//initialize data
		//$this->identity_column = $this->config->item('identity', 'traveler');
	}

	public function get($id = null)
	{
	    $where = $id === null ? '' : 'WHERE travelers.id = '.$this->db->escape($id);
		$query = $this->db->query('SELECT travelers.id, travelers.name, travelers.phone, travelers.reputation
		                            FROM travelers
		                            '. $where . '
		                            ORDER BY travelers.id ASC');
        $result = $query->result();

		return $result;
	}
}
