<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($id = null)
	{
	    $where = $id === null ? '' : 'WHERE routes.id = '.$this->db->escape($id);
		$query = $this->db->query('SELECT routes.id, routes.name
		                            FROM routes
		                            '. $where . '
		                            ORDER BY routes.name ASC');
        $result = $query->result();

		return $result;
	}
}
