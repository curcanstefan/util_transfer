<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($vehicle_id = null)
	{
	    $where = $vehicle_id === null ? '' : ' WHERE vehicles.id = ' . $this->db->escape($vehicle_id);
        return $this->getvehicles($where);
	}

	public function getVehicles($where = null)
	{
		$query = $this->db->query('SELECT vehicles.id,
                                            vehicles.name,
		                                    vehicles.type,
		                                    vehicles.registration_number,
		                                    vehicles.capacity
		                            FROM vehicles
		                            ' . $where . '
		                            ORDER BY vehicles.name ASC');
        return $query->result();
	}

	public function add($input)
	{
	    $data = [
	        'name'                 => $input['name'],
	        'type'                 => $input['type'],
	        'registration_number'  => $input['registration_number'],
            'capacity'             => $input['capacity'],
	        ];
        $this->db->insert('vehicles', $data);

        return ($this->db->affected_rows() != 1) ? false : true;
	}
}
