<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trip_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($trip_id = null)
	{
	    $where = $trip_id === null ? '' : ' WHERE trips.id = ' . $this->db->escape($trip_id);
        return $this->getTrips($where);
	}

	public function getByDay($day = null)
	{
	    // call get with WHERE
	    return $this->getTrips('WHERE DATE(start_date) = \''.$day.'\'');
	}

	public function getTrips($where = null)
	{
		$query = $this->db->query('SELECT trips.id,
											trips.title,
		                                    trips.start_date,
		                                    trips.end_date,
		                                    trips.vehicle_id,
		                                    trips.price,
		                                    vehicles.name AS vehicle_name,
		                                    vehicles.capacity AS vehicle_capacity,
											trips.driver_id,
											travelers.name AS driver_name,
											trips.route_id,
											routes.name AS route_name
		                            FROM trips
		                            INNER JOIN vehicles ON trips.vehicle_id = vehicles.id
		                            INNER JOIN travelers ON trips.driver_id = travelers.id
		                            INNER JOIN routes ON trips.route_id = routes.id
		                            ' . $where . '
		                            ORDER BY trips.start_date ASC');
        return $query->result();
	}

	public function get_test($id = null)
	{
	    return json_encode($this->get($id));
	}

	public function add($input)
	{
	    $startDate  = strtotime($input['start_date'].' '.$input['start_time']);
        $endDate    = date('Y-m-d H:i:s', $startDate + (3*60*60));
        $startDate  = date('Y-m-d H:i:s', $startDate);

		$tripId = $input['id'];
	    $data = [
	        'title'         => $input['title'],
	        'start_date'    => $startDate,
	        'end_date'      => $endDate,
	        'price'    		=> $input['price'],
	        'vehicle_id'    => $input['vehicle_id'],
	        'driver_id'    	=> $input['driver_id'],
	        'route_id'    	=> $input['route_id'],
	        ];
		if ($tripId) {
			$this->db->where('id', $tripId);
			$this->db->update('trips', $data);
		} else {
			$this->db->insert('trips', $data);
		}

        return $this->db->insert_id();
	}

	public function setRow($input, $overwrite = false)
	{
		$data = [
	        'trip_id'       => $input['trip_id'],
	        'traveler_id'   => $input['traveler_id'],
	        'total_price'   => $input['total_price'],
	        'seats'    		=> $input['seats'],
	        'paid'    		=> $input['paid'],
	        'from_city'    	=> $input['from_city'],
	        'to_city'    	=> $input['to_city'],
	        ];
		$result = $this->db->select('trip_id')
			->get_where(
				'trip_rows',
				[
					'trip_id' 		=> $data['trip_id'],
					'traveler_id'	=> $data['traveler_id'],
				])
			->row();

		if ($result && $overwrite) {
			$tripRowId = $result->trip_id;
			$this->db->where('trip_id', $data['trip_id']);
			$this->db->where('traveler_id', $data['traveler_id']);
			$this->db->update('trip_rows', $data);
			return $tripRowId;
		}

		if ($result) {
			// do not overwrite if user did not edit a traveler
			return 'duplicate';
			// throw exception 'duplicate'
		}

		$this->db->insert('trip_rows', $data);
        return $this->db->insert_id();
	}
}
