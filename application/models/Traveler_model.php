<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Traveler_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('traveler', TRUE);
	}

	public function get($id = null)
	{
	    $where = $id === null ? '' : 'WHERE travelers.id = '.$this->db->escape($id);
		$query = $this->db->query('SELECT travelers.id, travelers.name, travelers.phone, travelers.reputation
		                            FROM travelers
		                            '. $where . '
		                            ORDER BY travelers.name ASC');

		return $query->result();
	}

	public function getTravelersByTripId($tripId = null, $travelerId = null)
	{
	    $where = $tripId === null ? '' : 'WHERE trips.id = '.$this->db->escape($tripId);
	    $where .= $travelerId === null ? '' : ' AND travelers.id = '.$this->db->escape($tripId);
		$query = $this->db->query('SELECT travelers.id, travelers.name, travelers.phone, travelers.reputation,
										trip_rows.total_price,
										trip_rows.paid,
										trip_rows.seats,
										trip_rows.from_city,
										trip_rows.to_city
		                            FROM trips
		                            INNER JOIN trip_rows ON trips.id = trip_rows.trip_id
		                            INNER JOIN travelers ON trip_rows.traveler_id = travelers.id
		                            '. $where . '
		                            ORDER BY travelers.id ASC');

		return $query->result();
	}

	public function set($input)
	{
		$travelerId = $input['id'] + 0;
	    $data = [
	        'name'      => $input['name'],
	        'phone'    	=> $input['phone'],
	        'added_by'	=> $input['added_by'],
	        ];
		$result = $this->db->select('id')
			->get_where(
				'travelers',
				[
					'name' 		=> $data['name'],
					'phone' 	=> $data['phone'],
					'added_by'	=> $data['added_by'],
				])
			->row();
		if (!$result) {
			$this->db->insert('travelers', $data);
	        return $this->db->insert_id();
		}
		$travelerId = $result->id;

		return $travelerId;
	}
}
