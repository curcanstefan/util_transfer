<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="assets/favicon.ico">

        <title>calendar calatorii</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/yeti/bootstrap.min.css">-->
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">-->

        <link href="/assets/css/bootstrap-datepicker3.css" rel="stylesheet">
        <link href="/assets/css/custom.css" rel="stylesheet">
        <link href="/assets/css/ring.css" rel="stylesheet">

        <link href="/assets/css/material-fullpalette.min.css" rel="stylesheet">
        <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->

        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js" ></script>-->
<!--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <script src="/assets/js/bootstrap-datepicker.js"></script>
        <script src="/assets/js/bootstrap-datepicker.ro.min.js"></script>

<!--        <script src="/assets/js/material.min.js"></script>-->

        <script src="/assets/js/trip-schedule.js"></script>
        <script src="/assets/js/dataManager.js"></script>
        <script src="/assets/js/dictionary.js"></script>
        <script src="/assets/js/templates.js"></script>
        <script src="/assets/js/mainApp.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="well margin-top">
                <h1 id="selectedDate">
                    Choose a day
                </h1>
                <div id="date" class="panel-body">
                </div>
                <div id="datePopoverContainer"></div>
            </div>

            <div class="well">
                <h1 id="tripDetailsTitle">
                    Trip Details
                </h1>
                <div id="tripDetails"></div>
            </div>
            <div class="bigSpacer"></div>
        </div>
        <div style="display:none;" id="includedContent">
            <?php include(ROOT_DIR.'/assets/templates/templates.html'); ?>
        </div>

        <script type="text/javascript">
            function initDatePopover(selectedDate){
                var tripsForDay = schedule.getTripsByDate(selectedDate);
                var content = {};
                if(!tripsForDay || !tripsForDay.length){
                    content.errors = [
                        {'message': dictionary.get('no trips for this day')}
                    ];
                }
                tripsForDay.forEach(function(el){
                    el.displayStartDate = customTime.format(el.startDate,'time');
                    el.occupiedSeats = el.travelers.length;
                });
                content.trips = tripsForDay;

                // add button for adding new trip
                content.newTrips = [{
                    'date':     customTime.format(selectedDate, 'date'),
                    'title':    dictionary.get('add new trip'),
                }];

                var listTemplate = $('.xtemplate#listTripsForDay').html();
                $('.popover .popover-content').html(templates.render(listTemplate, content));
            }

            function onDateSelected(e){
                $('#selectedDate').html(customTime.format(e.date, 'serverDate'));
                var popoverContainer = $('.datepicker table tr td.active');
                var popoverTemplate = ['<div class="timePickerWrapper popover fresh">',
                    '<div class="arrow"></div>',
                    '<div class="popover-content">',
                    '</div>',
                    '</div>'].join('');
                $(popoverContainer).prop('data-trigger', 'focus');
                $('#datePopoverContainer').html('');

                $(popoverContainer).popover({
                    trigger: 'focus',
                    content: '<div class="uil-ring-css" style="transform:scale(0.24);"><div></div></div>',
                    template: popoverTemplate,
                    placement: "bottom",
                    html: true,
                    container: '#datePopoverContainer'
                });
                $(popoverContainer).popover('show');

                var callback = function() {
                    initDatePopover(e.date);
                };
                dataManager.getTripsByDay(e.date, callback);
            }

            $(document).ready(function(){
                // This command is used to initialize some elements and make them work properly
//                $.material.init();

                $('#date').on('click', function(){
                    $('#datePopoverContainer > div').not('.fresh').remove();
                    $('#datePopoverContainer .fresh').removeClass('fresh');
                });

                $('#date').datepicker({
                    startDate: "01/01/2015",
                    weekStart: 1,
                    todayBtn: "linked",
                    language: "ro",
                    todayHighlight: true/*,
                    datesDisabled: ['06/06/2015', '06/21/2015']*/
                })
                .on('changeDate', onDateSelected);

                $(window).trigger('hashchange');

            });
        </script>
    </body>
</html>
