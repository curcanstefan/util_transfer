<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/assets/css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">

      <form class="form-signin" action="login" method="POST">

      <h2 class="form-signin-heading"><?php echo lang('login_heading');?></h2>
      
      <div id="infoMessage"><?php echo $message;?></div>

        <div class="form-group">
          <label for="inputEmail" class="sr-only">
            <?php echo lang('login_identity_label', 'identity');?>
          </label>
          <input type="email" id="identity" name="identity" class="form-control" placeholder="Email address" required="" autofocus="" value="<?php echo $identity['value'];?>">
        </div>

        <div class="form-group">
          <label for="inputPassword" class="sr-only">
            <?php echo lang('login_password_label', 'password');?>
          </label>
          <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
        </div>

        <div class="checkbox">
          <label>
            <input id="remember" name="remember" type="checkbox" value="1">
            <?php echo lang('login_remember_label');?>
          </label>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo lang('login_submit_btn');?></button>

        <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
        
      </form>

    </div>
  </body>
</html>