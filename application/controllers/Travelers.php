<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travelers extends CI_Controller
{
	public function get($id = null)
	{
	    $this->load->model('traveler_model');
        $travelers = $this->traveler_model->get($id);

        echo json_encode($travelers);
	}

	public function set()
	{
	    $traveler = [
	        'id'         	=> $this->input->post('traveler_id'),
	        'name'         	=> $this->input->post('traveler_name'),
	        'phone'    		=> $this->input->post('phone'),
	        'added_by'    	=> 0,  // admin
	        ];
		$this->load->model('traveler_model');
		$travelerId = $this->traveler_model->set($traveler);

		$tripRow = [
	        'id'       		=> $this->input->post('trip_row_id'),
	        'trip_id'       => $this->input->post('trip_id'),
	        'traveler_id'   => $travelerId,
	        'seats'   		=> $this->input->post('seats'),
	        'total_price'   => $this->input->post('total_price'),
	        'paid'    		=> $this->input->post('amount_paid'),
	        'from_city'    	=> $this->input->post('departure_city'),
	        'to_city'    	=> $this->input->post('destination_city'),
	        ];
		$this->load->model('trip_model');
		$overwrite = $traveler['id'] + 0 > 0;
		$this->data['message'] = $this->trip_model->setRow($tripRow, $overwrite);

		if ($this->data['message'] === 'duplicate') {
			$this->output->set_status_header('400');
		}
		echo json_encode($this->data);
	}
}
