<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drivers extends CI_Controller
{
    public function get($id = null)
	{
		$this->load->model('driver_model');
		$drivers = $this->driver_model->get($id);
        echo json_encode($drivers);
	}
}
