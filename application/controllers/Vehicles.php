<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicles extends CI_Controller
{
	public function index()
	{
		$this->load->model('traveler_model');
		echo $this->traveler_model->get_test();
	}

	public function get($id = null)
	{
		$this->load->model('vehicle_model');
		$vehicles = $this->vehicle_model->get($id);
        echo json_encode($vehicles);
	}
}
