<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trips extends CI_Controller
{
	public function index()
	{
		$this->load->model('traveler_model');
		echo $this->traveler_model->get_test();
	}

	public function get($id = null)
	{
		$this->load->model('trip_model');
		$trips = $this->trip_model->get($id);
        foreach ($trips as $key => $trip) {
		    $this->load->model('traveler_model');
            $travelers = $this->traveler_model->getTravelersByTripId($trip->id);
            $trip->travelers = $travelers;
        }

        echo json_encode($trips);
	}

	public function getByDay($day = null)
	{
	    $this->load->model('trip_model');
		$trips = $this->trip_model->getByDay($day);
		foreach ($trips as $key => $trip) {
		    $this->load->model('traveler_model');
            $travelers = $this->traveler_model->getTravelersByTripId($trip->id);
            $trip->travelers = $travelers;
        }

        echo json_encode($trips);
	}

	public function set()
	{
	    $trip = [
	        'id'         	=> $this->input->post('id'),
	        'title'         => $this->input->post('title'),
	        'start_date'    => $this->input->post('start_date'),
	        'start_time'    => $this->input->post('start_time'),
	        'vehicle_id'    => $this->input->post('vehicle'),
	        'driver_id'    	=> $this->input->post('driver'),
	        'route_id'    	=> $this->input->post('route'),
	        ];
		$this->load->model('trip_model');
		echo $this->trip_model->add($trip);
	}

	public function getTraveler($tripId, $travelerId)
	{

	}
}
