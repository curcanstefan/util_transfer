<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends CI_Controller
{
	public function get($id = null)
	{
		$this->load->model('route_model');
		$routes = $this->route_model->get($id);
        echo json_encode($routes);
	}
}
