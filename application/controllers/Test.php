<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    //die('welcome......');
	   // $this->load->library('Migration');
	   // $this->migration->current();

		$this->load->library(array('ion_auth'));
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->load->view('trip');
	}

	public function sendEmail($subject)
	{
		$this->load->library(array('mail_engine'));
		echo json_encode($this->mail_engine->sendEmail('curcanstefan@gmail.com', 'Stefan', $subject, 'some html here', 'and some plain text here'));
	}

	public function sendSms()
	{
		$this->load->library(array('sms_engine'));
		echo json_encode($this->sms_engine->sendSms('0752409585', 'congrats on your sms right here'));
	}
}
